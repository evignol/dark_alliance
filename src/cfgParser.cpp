#include <iostream>
#include <fstream>
#include <irrlicht.h>
#include "cfgParser.hpp"

cfgParser::cfgParser(irr::IrrlichtDevice *device) : _device(device)
{}

cfgParser::cfgParser(irr::IrrlichtDevice *device, const std::string &cfgFile) : _device(device)
{
	this->loadFile(cfgFile);
}

cfgParser::~cfgParser(void)
{}

irr::core::vector2di		cfgParser::getVector2di(const std::string &key, irr::core::vector2di defValue)
{
	int				x = 0, y = 0;
	std::string		strX, strY;

	if (this->_data.find(key) == this->_data.end())
	{
		std::cerr << key << " not found in config." << std::endl;
		return defValue;
	}
	strX = std::string(this->_data[key], 0, this->_data[key].find(','));
	strY = std::string(this->_data[key], this->_data[key].rfind(',') + 1, this->_data[key].size());
	x = atof(strX.c_str());
	y = atof(strY.c_str());
	irr::core::vector2di	vec(x, y);
	return vec;
}

irr::core::vector3df	cfgParser::getVector3df(const std::string &key, irr::core::vector3df defValue)
{
	float			x = 0.0f, y = 0.0f, z = 0.0f;
	std::string		strX, strY, strZ;

	if (this->_data.find(key) == this->_data.end())
		return defValue;
	strX = std::string(this->_data[key], 0, this->_data[key].find(','));
	strY = std::string(this->_data[key], this->_data[key].find(',') + 1, this->_data[key].rfind(','));
	strZ = std::string(this->_data[key], this->_data[key].rfind(',') + 1, this->_data[key].size());
	x = atof(strX.c_str());
	y = atof(strY.c_str());
	z = atof(strZ.c_str());
	irr::core::vector3df	vec(x, y, z);
	return vec;
}

float					cfgParser::getFloat(const std::string &key, float defValue)
{
	float			value = 0.0f;
	std::string		strValue;

	if (this->_data.find(key) == this->_data.end())
		return defValue;
	strValue = std::string(this->_data[key], 0, this->_data[key].find('\n'));
	value = atof(strValue.c_str());
	return value;
}

int						cfgParser::getInt(const std::string &key, int defValue)
{
	int				value = 0;
	std::string		strValue;

	if (this->_data.find(key) == this->_data.end())
		return defValue;
	strValue = std::string(this->_data[key], 0, this->_data[key].find('\n'));
	value = atoi(strValue.c_str());
	return value;
}

bool					cfgParser::getBool(const std::string &key, bool defValue)
{
	bool			value = false;
	std::string		strValue;

	if (this->_data.find(key) == this->_data.end())
		return defValue;
	strValue = std::string(this->_data[key], 0, this->_data[key].find('\n'));
	if (strValue == "true")
		value = true;
	return value;
}

std::string				cfgParser::getString(const std::string &key, std::string defValue)
{
	std::string		value;

	if (this->_data.find(key) == this->_data.end())
		return defValue;
	value = std::string(this->_data[key], this->_data[key].find('=') + 1, this->_data[key].size());
	return value;
}

bool	readline(irr::io::IReadFile *f, irr::core::stringc *str)
{
	char	ch;

	*str = "";
	while (f->read(&ch, 1) != 0)
	{
		if (ch == '\n')
			return true;
		else
			*str += ch;
	}
	return false;
}

void					cfgParser::loadFile(const std::string &cfgFile)
{
	std::string				line, key, value;
	irr::core::stringc		str;
	irr::io::IFileSystem	*filesys = NULL;
	irr::io::IReadFile		*file = NULL;

	if (!this->_device)
	{
		std::ifstream	file(cfgFile.c_str(), std::ios::in);

		if (!file)
		{
			std::cerr << "Error while opening config file: " << cfgFile << std::endl;
			return;
		}
		while (getline(file, line, '\n'))
		{
			key = std::string(line, 0, line.find('='));
			if (key[0] != COMMENT)
			{
				value = std::string(line, line.find('=') + 1, line.size());
				this->_data[key] = value;
			}
		}
		file.close();
		return;
	}
	//Read using irrlicht FS - needed when using zip files
	filesys = this->_device->getFileSystem();
	if (!filesys->existFile(cfgFile.c_str()))
	{
		std::cerr << "File not found " << cfgFile << std::endl;
		return;
	}
	file = filesys->createAndOpenFile(cfgFile.c_str());
	if (!file)
	{
		std::cerr << "Error while opening config file: " << cfgFile << std::endl;
		return;
	}
	while (readline(file, &str))
	{
		key = std::string(str.c_str(), 0, str.findFirst('='));
		if (key[0] != COMMENT)
		{
			value = std::string(str.c_str(), str.findFirst('=') + 1, str.size());
			this->_data[key] = value;
		}
	}
	if (str != "")
	{
		key = std::string(str.c_str(), 0, str.findFirst('='));
		if (key[0] != COMMENT)
		{
			value = std::string(str.c_str(), str.findFirst('=') + 1, str.size());
			this->_data[key] = value;
		}
	}
	file->drop();
}