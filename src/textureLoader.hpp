#ifndef TEXTURELOADER_HPP
# define TEXTURELOADER_HPP

# include <irrlicht.h>

irr::video::ITexture		*LoadTextureFromImage(irr::IrrlichtDevice *device, irr::video::IVideoDriver *driver, const irr::c8 *filename, const irr::core::dimension2d<irr::s32> size, const bool keepAspectRatio=false);

#endif