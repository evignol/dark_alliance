#ifndef MAP_HPP
# define MAP_HPP

# include <string>
# include <vector>
# include <irrlicht.h>
# include "APlayer.hpp"

class	Map
{
	public:
		Map(irr::IrrlichtDevice *device, irr::video::IVideoDriver *driver, irr::scene::ISceneManager *smgr, irr::scene::ICameraSceneNode *camera, const std::string &file, int textureSize, int screenWidth, int screenHeight);
		~Map(void);
		void									addPlayer(APlayer *playerToAdd);

	private:
		irr::IrrlichtDevice						*_device;
		irr::video::IVideoDriver				*_driver;
		irr::scene::ISceneManager				*_smgr;
		irr::scene::ICameraSceneNode			*_camera;
		irr::scene::ITriangleSelector			*_triangleSelector;
		irr::scene::IAnimatedMesh				*_mesh;
		irr::scene::ISceneNode					*_node;
		std::vector<APlayer *>					*_players;
		std::vector<irr::video::ITexture *>		*_textures;
		irr::core::vector3df					_playerStartPos;

		void									_dispLoadingScreen(const std::string &imageFile, int screenWidth, int screenHeight) const;
};

#endif
