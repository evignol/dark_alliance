#ifndef CFGPARSER_HPP
# define CFGPARSER_HPP

# include <string>
# include <map>
# include <irrlicht.h>

# define COMMENT	'#'

class	cfgParser
{
	public:
		cfgParser(irr::IrrlichtDevice *device);
		cfgParser(irr::IrrlichtDevice *device, const std::string &cfgFile);
		~cfgParser(void);
		void									loadFile(const std::string &cfgFile);
		irr::core::vector3df					getVector3df(const std::string &key, irr::core::vector3df defValue);
		float									getFloat(const std::string &key, float defValue);
		int										getInt(const std::string &key, int defValue);
		bool									getBool(const std::string &key, bool defValue);
		std::string								getString(const std::string &key, std::string defValue);
		irr::core::vector2di					getVector2di(const std::string &key, irr::core::vector2di defValue);

	private:
		std::map<std::string, std::string>		_data;
		irr::IrrlichtDevice						*_device;
};

#endif