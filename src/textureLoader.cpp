#include <irrlicht.h>

irr::video::ITexture	*LoadTextureFromImage(irr::IrrlichtDevice *device, irr::video::IVideoDriver *driver, const irr::c8 *filename, const irr::core::dimension2d<irr::s32> size, const bool keepAspectRatio=false)
{
	irr::video::ITexture				*result_texture = NULL;
	irr::video::IImage					*im1, *im2;
	irr::core::dimension2d<irr::u32>	im1size;
	irr::core::dimension2di				im2size;
	irr::s32							*im2buf = NULL;

	im1 = device->getVideoDriver()->createImageFromFile(filename);
	if (im1)
	{
		im1size = im1->getDimension();
		if (im1size.Width <= size.Width || im1size.Height <= size.Height)
		{
			im1->drop();
			return NULL;
		}
		if (im1size.Width > 0 && im1size.Height > 0)
		{
			im2size = size;
			if (keepAspectRatio)
			{
				if (im1size.Width > im1size.Height)
				{
					im2size.Height = (im1size.Height * size.Width) / im1size.Width;
					im2size.Width = size.Width;
				}
				else
				{
					im2size.Width = (im1size.Width * size.Height) / im1size.Height;
					im2size.Height = size.Height;
				}
			}
			im2buf = (irr::s32*)malloc(sizeof(irr::s32) * im2size.Width * im2size.Height);
			if (im2buf)
			{
				im2 = driver->createImageFromData(irr::video::ECF_A8R8G8B8, irr::core::dimension2d<irr::u32>(im2size.Width, im2size.Height), im2buf);
				if (im2)
				{
					im1->copyToScaling(im2);
					result_texture = device->getVideoDriver()->addTexture(filename, im2);
					im2->drop();
				}
				free(im2buf);
			}
		}
		im1->drop();
	}
	return result_texture;
}
