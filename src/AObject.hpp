#ifndef AOBJECT_HPP
# define AOBJECT_HPP

# include <irrlicht.h>

class	AObject
{
	public:
		AObject(irr::IrrlichtDevice *device, irr::video::IVideoDriver *driver, irr::scene::ISceneManager *smgr, std::string &name, irr::core::vector3df &pos);
		~AObject(void);

	private:
		irr::IrrlichtDevice					*_device;
		irr::video::IVideoDriver			*_driver;
		irr::scene::ISceneManager			*_smgr;
		irr::scene::IAnimatedMesh			*_mesh;
		irr::scene::IAnimatedMeshSceneNode	*_node;
		irr::video::ITexture				*_diffuse, *_normal;
};

#endif