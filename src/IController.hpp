#ifndef ICONTROLLER_HPP
# define ICONTROLLER_HPP

# include <string>
# include <irrlicht.h>
# include "APlayer.hpp"

class	IController
{
	public:
		virtual void	refresh(void) = 0;
		virtual void	setPlayer(APlayer *player) = 0;
		virtual void	loadConfig(std::string const &cfgFile) = 0;
		virtual void	melee_attack(irr::f32 val1, irr::f32 val2) = 0;
		virtual void	heavy_attack(irr::f32 val1, irr::f32 val2) = 0;
		virtual void	magical_attack(irr::f32 val1, irr::f32 val2) = 0;
};

#endif