#include <iostream>
#include "Map.hpp"
#include "textureLoader.hpp"
#include "cfgParser.hpp"

Map::Map(irr::IrrlichtDevice *device, irr::video::IVideoDriver *driver, irr::scene::ISceneManager *smgr, irr::scene::ICameraSceneNode *camera, const std::string &file, int textureSize, int screenWidth, int screenHeight) : _device(device), _driver(driver), _smgr(smgr), _camera(camera), _triangleSelector(NULL), _mesh(NULL), _node(NULL), _players(NULL)
{
	std::string				fileName, splashScreen("maps/levelshots/"), dir("maps/"), pakExt(".pk3"), mapExt(".bsp");
	irr::video::ITexture	*texture = NULL, *scaledTexture = NULL;
	irr::core::vector3df	defPos(0.0f, 0.0f, 0.0f), mapPos;
	cfgParser				config(device);

	fileName = dir + file + pakExt;
	device->getFileSystem()->addFileArchive(fileName.c_str());
	//Loading screen
	this->_dispLoadingScreen(splashScreen + file + ".jpg", screenWidth, screenHeight);

	config.loadFile("config/" + file + ".cfg");
	mapPos = config.getVector3df("MAP_POS", defPos);
	this->_playerStartPos = config.getVector3df("PLAYER_START_POS", defPos);
	fileName = file + mapExt;
	this->_mesh = this->_smgr->getMesh(fileName.c_str());
	if (!this->_mesh)
		return;
	this->_node = this->_smgr->addOctreeSceneNode(this->_mesh->getMesh(0), 0, 1 << 0, 1024);
	if (!this->_node)
		return;
	this->_node->setPosition(mapPos);
	unsigned int	size = 0, scaledSize = 0;
	this->_textures = new std::vector<irr::video::ITexture *>;
	for (unsigned short i = 0; i < this->_node->getMaterialCount(); i++)
	{
		texture = this->_node->getMaterial(i).getTexture(0);
		if (texture)
		{
			std::string		name(texture->getName().getPath().c_str());
			irr::core::dimension2d<unsigned int>	textureDimensions = texture->getSize();
			if (textureDimensions.Width > textureSize)
			{
				scaledTexture = LoadTextureFromImage(this->_device, this->_driver, name.c_str(), irr::core::dimension2d<irr::s32>(textureSize, textureSize));
				if (scaledTexture)
				{
					this->_textures->push_back(texture);
					size += scaledTexture->getPitch() * textureSize;
					this->_node->getMaterial(i).setTexture(0, scaledTexture);
				}
			}
			else
				size += texture->getPitch() * textureDimensions.Height;
		}
	}
	std::vector<irr::video::ITexture *>::iterator	it, end = this->_textures->end();
	for (it = this->_textures->begin(); it != end; it++)
		this->_driver->removeTexture(*it);
	std::cout << "Map use " << size / (1024 * 1024) << "Mo for " << this->_driver->getTextureCount() << " textures." << std::endl;
	this->_players = new std::vector<APlayer *>;
	this->_triangleSelector = this->_smgr->createOctreeTriangleSelector(this->_mesh->getMesh(0), this->_node, 128);
	if (!this->_triangleSelector)
		return;
	this->_node->setTriangleSelector(this->_triangleSelector);
}

Map::~Map(void)
{
	for (short i = 0; i < this->_node->getMaterialCount(); i++)
		this->_driver->removeTexture(this->_node->getMaterial(i).getTexture(0));
	this->_node->removeAll();
	delete this->_players;
}

void						Map::addPlayer(APlayer *playerToAdd)
{
	if (this->_triangleSelector)
	{
		irr::scene::ISceneNodeAnimator	*anim = this->_smgr->createCollisionResponseAnimator(
			this->_triangleSelector, playerToAdd->getNode(), playerToAdd->getCollRadius(),
			playerToAdd->getCollOffset(), playerToAdd->getCollOffset());
		this->_triangleSelector->drop();
		playerToAdd->getNode()->addAnimator(anim);
		anim->drop();
		playerToAdd->getNode()->setPosition(this->_playerStartPos);
		irr::core::vector3df	camPos(this->_playerStartPos);
		camPos.Y += 200.0f;
		camPos.Z -= 100.0f;
		this->_camera->setPosition(camPos);
		this->_camera->setTarget(this->_playerStartPos);
	}
	this->_players->push_back(playerToAdd);
}

void						Map::_dispLoadingScreen(const std::string &imageFile, int screenWidth, int screenHeight) const
{
	irr::video::ITexture		*texture = NULL;
	irr::gui::IGUIEnvironment	*guienv = NULL;
	irr::gui::IGUIImage			*img = NULL;

	texture = this->_driver->getTexture(imageFile.c_str());
	this->_driver->beginScene(true, true, irr::video::SColor(255, 42, 42, 42));
	guienv = this->_device->getGUIEnvironment();
	img = guienv->addImage(irr::core::rect<irr::s32>(0, 0, screenWidth, screenHeight));
	img->setImage(texture);
	img->setScaleImage(true);
	this->_driver->removeTexture(texture);
	guienv->drawAll();
	this->_driver->endScene();
}
