#ifndef GAME_HPP
# define GAME_HPP

# include <string>
# include <irrlicht.h>
# include "IController.hpp"
# include "AController.hpp"
# include "cfgParser.hpp"
# include "Map.hpp"

# define FPS_LIMIT		1000.0f / 60.0f
# define NB_PLAYERS		4

enum
{
	ID_IsNotPickable = 0,
	IDFlag_IsPickable = 1 << 0
};

class	Game
{
	public:
		Game(void);
		~Game(void);
		void								loadController(const std::string &libName);
		void								render(void);

	private:
		irr::SIrrlichtCreationParameters	*_p;
		AController							*_receiver;
		IController							*_controller;
		irr::IrrlichtDevice					*_device;
		irr::video::IVideoDriver			*_driver;
		irr::scene::ISceneManager			*_smgr;
		irr::scene::ICameraSceneNode		*_camera;
		Map									*_map;
		APlayer								*_players[NB_PLAYERS];
		int									_screenWidth, _screenHeight, _textureSize;
		irr::u32							_now, _then;
		int									_lastFPS, _fps;
		irr::video::E_DRIVER_TYPE			_driverType;
		std::string							_inputModule;

		void								*_controllerHandle;
		IController							*(*_controllerCreator)(irr::IrrlichtDevice *, AController *);
		void								(*_deleteController)(IController *);
		void								_dlErrorWrapper(void) const;
		void								_getConfig(const std::string &cfgFile);
		void								_loadMap(const std::string &mapFile);
		void								_getInput(void);
};

#endif