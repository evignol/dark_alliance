#include <iostream>
#include "textureLoader.hpp"
#include "cfgParser.hpp"
#include "APlayer.hpp"

APlayer::APlayer(irr::IrrlichtDevice *device, irr::video::IVideoDriver *driver, irr::scene::ISceneManager *smgr, irr::scene::ICameraSceneNode *camera, const std::string &name, const irr::core::vector3df &pos, int textureSize) : _MOVEMENT_SPEED(0.f), _offset(180.0f), _device(device), _driver(driver), _smgr(smgr), _camera(camera), _mesh(NULL), _node(NULL), _diffuse(NULL), _normal(NULL), _then(0), _oldHoryzontal(0.f), _oldVertical(0.f), _state(STATE_IDLE)
{
	std::string				dir, pk3_file, mesh_file, diffuse_file, normal_file, configFile;
	cfgParser				config(device);
	irr::core::vector3df	scale;
	irr::core::vector2di	defAnimRange(0, 1);

	//Prepare vars
	dir = "meshes/" + name + "/";
	pk3_file = "meshes/" + name + ".pk3";
	mesh_file = dir + name + ".X";
	diffuse_file = dir + "diffuse.jpg";
	normal_file = dir + "normal.jpg";
	configFile = "meshes/" + name + "/" + name + ".cfg";

	this->_then = device->getTimer()->getTime();
	//Load PAK file
	device->getFileSystem()->addFileArchive(pk3_file.c_str());
	//Load config
	config.loadFile("meshes/" + name + "/" + name + ".cfg");
	scale = config.getVector3df("SCALE", irr::core::vector3df(1.0f, 1.0f, 1.0f));
	this->_collOffset = config.getVector3df("OFFSET", irr::core::vector3df(0.0f, 0.0f, 0.0f));
	this->_collRadius = config.getVector3df("COLL_RADIUS", irr::core::vector3df(0.0f, 0.0f, 0.0f));
	this->_FPS = config.getFloat("FPS", 25.0f);
	this->_MOVEMENT_SPEED = config.getFloat("MOVEMENT_SPEED", 200.0f);
	this->_animIdle = config.getVector2di("ANIM_IDLE", defAnimRange);
	this->_animWalk = config.getVector2di("ANIM_WALK", defAnimRange);
	this->_animRun = config.getVector2di("ANIM_RUN", defAnimRange);
	this->_animStrafe = config.getVector2di("ANIM_STRAFE", defAnimRange);
	this->_animMeleeAttack = config.getVector2di("ANIM_MELEE_ATTACK", defAnimRange);
	this->_animHeavyAttack = config.getVector2di("ANIM_HEAVY_ATTACK", defAnimRange);
	this->_animMagicalAttack = config.getVector2di("ANIM_MAGICAL_ATTACK", defAnimRange);
	//Load ressources
	this->_mesh = smgr->getMesh(mesh_file.c_str());
	this->_node = smgr->addAnimatedMeshSceneNode(this->_mesh);
	this->_diffuse = LoadTextureFromImage(device, driver, diffuse_file.c_str(), irr::core::dimension2d<irr::s32>(textureSize, textureSize));
	this->_normal = LoadTextureFromImage(device, driver, normal_file.c_str(), irr::core::dimension2d<irr::s32>(textureSize, textureSize));
	if (this->_diffuse)
		this->_node->getMaterial(0).setTexture(0, this->_diffuse);
	if (this->_normal)
		this->_node->getMaterial(0).setTexture(1, this->_normal);
	else
		this->_node->setMaterialType(irr::video::EMT_SOLID);
	//Set properties
	this->_node->setScale(scale);
	this->_node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
	this->_node->setPosition(pos);
	this->_node->setAnimationSpeed(this->_FPS);
	this->_node->setFrameLoop(this->_animIdle.X, this->_animIdle.Y);
	this->_node->setID(1 << 0);
}

APlayer::~APlayer(void)
{
	if (this->_diffuse)
		this->_diffuse->drop();
	if (this->_normal)
		this->_normal->drop();
	/*if (this->_diffuse)
		this->_driver->removeTexture(this->_diffuse);
	if (this->_normal)
		this->_driver->removeTexture(this->_normal);*/
	this->_node->removeAll();
}

void		APlayer::refresh(void)
{
	if (this->_state == STATE_STRAFE)
		this->strafe(1.0f, 1.0f);
	//std::cout << "STATE => " << this->_state << std::endl;
}

void		APlayer::melee_attack(void)
{
	if (this->_state == STATE_IDLE)
	{
		this->_node->setLoopMode(false);
		this->_node->setAnimationEndCallback(this);
		this->_node->setFrameLoop(this->_animMeleeAttack.X, this->_animMeleeAttack.Y);
		this->_state = STATE_LIGHT_ATTACK;
	}
}

void		APlayer::OnAnimationEnd(irr::scene::IAnimatedMeshSceneNode *node)
{
	this->_node->setLoopMode(true);
	this->_node->setAnimationSpeed(this->_FPS);
	this->_node->setFrameLoop(this->_animIdle.X, this->_animIdle.Y);
	this->_state = STATE_IDLE;
}

void		APlayer::heavy_attack(void)
{
	if (this->_state == STATE_IDLE)
	{
		this->_node->setLoopMode(true);
		this->_node->setFrameLoop(this->_animHeavyAttack.X, this->_animHeavyAttack.Y);
		this->_state = STATE_HEAVY_ATTACK;
	}
}

void		APlayer::magical_attack(void)
{
	if (this->_state == STATE_IDLE)
	{
		this->_node->setLoopMode(true);
		this->_node->setFrameLoop(this->_animMagicalAttack.X, this->_animMagicalAttack.Y);
		this->_state = STATE_MAGICAL_ATTACK;
	}
}

void		APlayer::strafe(irr::f32 horizontal, irr::f32 vertical)
{
	irr::core::vector3df	pos;
	static irr::f32			x = 0.f;
	static irr::f32			oldHor = 0.f, oldVer = 0.f;
	irr::f32				strafeHor, strafeVer;

	if (this->_state == STATE_STRAFE)
	{
		x++;
		pos = this->_node->getPosition();
		strafeHor = fabs((sin(x) * oldHor * 1.5f));
		strafeVer = fabs((sin(x) * oldVer * 1.5f));
		if (oldHor < 0.f)
			pos.X -= strafeHor;
		else
			pos.X += strafeHor;
		if (oldVer < 0.f)
			pos.Z += strafeVer;
		else
			pos.Z -= strafeVer;
		this->_node->setPosition(pos);
		if (x > 80.0f)
			this->_state = STATE_IDLE;
		return;
	}
	if (this->_state != STATE_STRAFE)
	{
		x = 0.0f;
		oldHor = horizontal;
		oldVer = vertical;
		this->_node->setLoopMode(false);
		this->_node->setAnimationEndCallback(this);
		this->_node->setFrameLoop(this->_animStrafe.X, this->_animStrafe.Y);
		this->_state = STATE_STRAFE;
	}
}

void		APlayer::move(irr::f32 horyzontal, irr::f32 vertical)
{
	irr::core::vector3df	pos, rot;
	irr::f32				speed = 0.f;
	const irr::u32			now = this->_device->getTimer()->getTime();
	const irr::f32			frameDeltaTime = (irr::f32)(now - this->_then) / 1000.f;

	this->_then = now;
	if (horyzontal < 0.f)
		speed = -horyzontal;
	else
		speed = horyzontal;
	if (vertical < 0.f)
		speed += -vertical;
	else
		speed += vertical;
	if (irr::core::equals(horyzontal, 0.f) && irr::core::equals(vertical, 0.f))
	{
		if (this->_state == STATE_WALK || this->_state == STATE_RUN || this->_state == STATE_ROTATE)
		{
			this->_node->setLoopMode(true);
			this->_node->setAnimationSpeed(this->_FPS);
			this->_node->setFrameLoop(this->_animIdle.X, this->_animIdle.Y);
			this->_state = STATE_IDLE;
		}
		return;
	}
	else
	{
		if (speed >= -0.3f && speed <= 0.3f)
			this->_state = STATE_ROTATE;
		else if (speed >= -0.5f && speed <= 0.5f)
		{
			if (this->_state != STATE_WALK)
			{
				this->_node->setFrameLoop(this->_animWalk.X, this->_animWalk.Y);
				this->_node->setAnimationSpeed(this->_FPS * speed);
				this->_state = STATE_WALK;
			}
		}
		else
		{
			if (this->_state != STATE_RUN)
			{
				this->_node->setFrameLoop(this->_animRun.X, this->_animRun.Y);
				this->_node->setAnimationSpeed(this->_FPS * speed);
				this->_state = STATE_RUN;
			}
		}
	}

	if (this->_state != STATE_ROTATE)
	{
		pos = this->_node->getPosition();
		//Move
		pos.X += this->_MOVEMENT_SPEED * frameDeltaTime * horyzontal;
		pos.Z += this->_MOVEMENT_SPEED * frameDeltaTime * vertical;
		this->_node->setPosition(pos);
		irr::core::vector3df	camPos(pos);
		camPos.Y += 200.0f;
		camPos.Z -= 100.0f;
		this->_camera->setPosition(camPos);
		this->_camera->setTarget(pos);
	}
	rot = this->_node->getRotation();
	//Rotation
	if (vertical < 0.f)
	{
		rot.Y = horyzontal * -90.0f;
		rot.Y -= 180.0f;
	}
	else
		rot.Y = horyzontal * 90.0f;
	rot.Y += this->_offset;
	this->_node->setRotation(rot);
}

irr::f32				APlayer::getOffset(void) const
{
	return this->_offset;
}

irr::core::vector3df	APlayer::getCollOffset(void) const
{
	return this->_collOffset;
}

irr::core::vector3df	APlayer::getCollRadius(void) const
{
	return this->_collRadius;
}

irr::scene::IAnimatedMeshSceneNode	*APlayer::getNode(void) const
{
	return this->_node;
}

void					APlayer::setPos(const irr::core::vector3df &pos)
{
	this->_node->setPosition(pos);
}

irr::core::vector3df	APlayer::getPos(void) const
{
	return this->_node->getPosition();
}
