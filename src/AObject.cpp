#include <iostream>
#include "AObject.hpp"

AObject::AObject(irr::IrrlichtDevice *device, irr::video::IVideoDriver *driver, irr::scene::ISceneManager *smgr, std::string &name, irr::core::vector3df &pos) : _device(device), _driver(driver), _smgr(smgr), _mesh(NULL), _node(NULL), _diffuse(NULL)
{
	std::string		dir, pk3_file, mesh_file, diffuse_file, normal_file;

	//Prepare vars
	dir = "meshes/" + name + "/";
	pk3_file = "meshes/" + name + ".pk3";
	mesh_file = dir + name + ".X";
	diffuse_file = dir + "diffuse.jpg";
	normal_file = dir + "normal.jpg";

	//Load the ressources
	device->getFileSystem()->addFileArchive(pk3_file.c_str());
	this->_mesh = smgr->getMesh(mesh_file.c_str());
	this->_node = smgr->addAnimatedMeshSceneNode(this->_mesh);
	this->_diffuse = driver->getTexture(diffuse_file.c_str());
	this->_normal = driver->getTexture(normal_file.c_str());
	if (this->_diffuse)
		this->_node->getMaterial(0).setTexture(0, this->_diffuse);
	if (this->_normal)
		this->_node->getMaterial(0).setTexture(1, this->_normal);
	else
		this->_node->setMaterialType(irr::video::EMT_SOLID);
	this->_node->setScale(irr::core::vector3df(0.75f, 0.75f, 0.75f));
	this->_node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
	this->_node->setPosition(pos);
}

AObject::~AObject(void)
{
	this->_node->removeAll();
}