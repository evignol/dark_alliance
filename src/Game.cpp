#include <dlfcn.h>
#include "driverChoice.h"
#include "Game.hpp"

Game::Game(void) : _receiver(NULL), _controller(NULL), _device(NULL), _driver(NULL), _smgr(NULL), _camera(NULL), _map(NULL), _screenWidth(800), _screenHeight(600), _textureSize(256), _now(0), _then(0), _lastFPS(-1), _fps(0)
{
	unsigned char	i;

	for (i = 0; i < NB_PLAYERS; i++)
		this->_players[i] = NULL;
	this->_driverType = irr::driverChoiceConsole();
	if (this->_driverType == irr::video::EDT_COUNT)
	{
		std::cerr << "Error while creating driver." << std::endl;
		return;
	}
	this->_receiver = new AController();
	this->_getConfig("default.cfg");
	this->_device = createDeviceEx(*this->_p);
	if (!this->_device)
	{
		std::cerr << "Error while creating device." << std::endl;
		return;
	}
	this->_device->setResizable(false);
	this->_device->setWindowCaption(L"Dark Alliance");
	this->_driver = this->_device->getVideoDriver();
	this->_smgr = this->_device->getSceneManager();
	this->_camera = this->_smgr->addCameraSceneNode(0, irr::core::vector3df(0, 15, -5),
									irr::core::vector3df(50, 0, 50), ID_IsNotPickable);
	//TEST
	this->_loadMap("20kdm2");
	this->_players[0] = new APlayer(this->_device, this->_driver, this->_smgr, this->_camera, "Taurus", irr::core::vector3df(0, 0, 0), this->_textureSize);
	this->_map->addPlayer(this->_players[0]);
	//Controller
	if (this->_inputModule != "default")
		this->loadController(this->_inputModule);
	if (this->_controller)
		this->_controller->setPlayer(this->_players[0]);
}

Game::~Game(void)
{
	unsigned char	i;

	delete this->_p;
	if (this->_controller)
		delete this->_controller;
	if (this->_map)
		delete this->_map;
	for (i = 0; i < NB_PLAYERS; i++)
		if (this->_players[i])
			delete this->_players[i];
}

void	Game::_dlErrorWrapper(void) const
{
	std::cerr << "Error: " << dlerror() << std::endl;
	exit(-1);
}

void	Game::loadController(const std::string &libName)
{
	std::cout << "Controller set to " << libName << std::endl;
	if (this->_controller)
	{
		std::cout << "Unloading previous controller.. ";
		this->_deleteController(this->_controller);
		dlclose(this->_controllerHandle);
		std::cout << "done." << std::endl;
	}
	this->_controllerHandle = dlopen(libName.c_str(), RTLD_LAZY | RTLD_LOCAL);
	if (!this->_controllerHandle)
		this->_dlErrorWrapper();
	this->_controllerCreator = (IController *(*)(irr::IrrlichtDevice *, AController *)) dlsym(this->_controllerHandle, "createController");
	this->_deleteController = (void(*)(IController *)) dlsym(this->_controllerHandle, "deleteController");
	if (!this->_controllerCreator || !this->_deleteController)
	this->_dlErrorWrapper();
	this->_controller = this->_controllerCreator(this->_device, this->_receiver);
}

void	Game::_getConfig(const std::string &cfgFile)
{
	int				bpp, antiAlias, zBuffer;
	bool			vSync, fullScreen;
	std::string		loggingLevel, deviceType;
	cfgParser		config(this->_device, cfgFile);

	this->_screenWidth = config.getInt("screenWidth", 640);
	this->_screenHeight = config.getInt("screenHeight", 480);
	bpp = config.getInt("bpp", 16);
	antiAlias = config.getInt("antiAlias", 0);
	loggingLevel = config.getString("loggingLevel", "default");
	deviceType = config.getString("deviceType", "best");
	vSync = config.getBool("vSync", false);
	fullScreen = config.getBool("fullScreen", false);
	zBuffer = config.getInt("zBuffer", 16);
	this->_inputModule = config.getString("controller", "default");
	this->_textureSize = config.getInt("max_texture_size", 256);
	//
	this->_p = new irr::SIrrlichtCreationParameters();
	this->_p->DriverType = this->_driverType;
	this->_p->WindowSize = irr::core::dimension2d<irr::u32>(this->_screenWidth, this->_screenHeight);
	this->_p->AntiAlias = antiAlias;
	this->_p->Bits = bpp;
	this->_p->Fullscreen = fullScreen;
	this->_p->ZBufferBits = zBuffer;
	this->_p->EventReceiver = this->_receiver;
	if (deviceType == "X11")
		this->_p->DeviceType = irr::EIDT_X11;
	if (deviceType == "FB")
		this->_p->DeviceType = irr::EIDT_FRAMEBUFFER;
	if (deviceType == "SDL")
		this->_p->DeviceType = irr::EIDT_SDL;
	if (loggingLevel == "debug")
		this->_p->LoggingLevel = irr::ELL_DEBUG;
	if (loggingLevel == "information")
		this->_p->LoggingLevel = irr::ELL_INFORMATION;
	if (loggingLevel == "warning")
		this->_p->LoggingLevel = irr::ELL_WARNING;
	if (loggingLevel == "error")
		this->_p->LoggingLevel = irr::ELL_ERROR;
	if (loggingLevel == "none")
		this->_p->LoggingLevel = irr::ELL_NONE;
}

void	Game::_getInput(void)
{
	irr::f32		horizontal = 0.f, vertical = 0.f;

	if (this->_players[0])
		this->_players[0]->refresh();
	if (this->_controller)
		this->_controller->refresh();
	else if (this->_players[0])
	{
		//Move
		if (this->_receiver->KeyIsDown[irr::KEY_UP])
			vertical = 0.9f;
		if (this->_receiver->KeyIsDown[irr::KEY_DOWN])
			vertical = -0.9f;
		if (this->_receiver->KeyIsDown[irr::KEY_LEFT])
			horizontal = -0.9f;
		if (this->_receiver->KeyIsDown[irr::KEY_RIGHT])
			horizontal = 0.9f;
		//Strafe
		if (this->_receiver->KeyIsDown[irr::KEY_KEY_Q])
			horizontal = -1.0f;
		if (this->_receiver->KeyIsDown[irr::KEY_KEY_D])
			horizontal = 1.0f;
		if (this->_receiver->KeyIsDown[irr::KEY_KEY_Z])
			vertical = 1.0f;
		if (this->_receiver->KeyIsDown[irr::KEY_KEY_S])
			vertical = -1.0f;
		if (irr::core::equals(horizontal, -1.f) || irr::core::equals(horizontal, 1.f)
			|| irr::core::equals(vertical, -1.f) || irr::core::equals(horizontal, 1.f))
			this->_players[0]->strafe(horizontal, vertical);
		else
			this->_players[0]->move(horizontal, vertical);
		//Attack
		if (this->_receiver->KeyIsDown[irr::KEY_SPACE])
			this->_players[0]->melee_attack();
		if (this->_receiver->KeyIsDown[irr::KEY_CONTROL])
			this->_players[0]->heavy_attack();
		if (this->_receiver->KeyIsDown[irr::KEY_LSHIFT])
			this->_players[0]->magical_attack();
	}
}

void	Game::render(void)
{
	while (this->_device->run() && !this->_receiver->KeyIsDown[irr::KEY_ESCAPE])
	{
		this->_fps = this->_driver->getFPS();
		this->_now = this->_device->getTimer()->getTime();
		if (this->_now > this->_then)
		{
			this->_then = this->_now + FPS_LIMIT;
			this->_getInput();
			if (this->_map && this->_receiver->KeyIsDown[irr::KEY_RETURN])
			{
				delete this->_map;
				this->_map = NULL;
			}
			else
			{
				this->_driver->beginScene(true, true, irr::video::SColor(255, 113, 113, 133));
				this->_smgr->drawAll();
				this->_driver->endScene();
			}
			if (this->_lastFPS != this->_fps)
			{
				irr::core::stringw str = L"Dark Alliance - [";
				str += this->_driver->getName();
				str += "] FPS:";
				str += this->_fps;
				this->_device->setWindowCaption(str.c_str());
				this->_lastFPS = this->_fps;
			}
		}
		this->_device->sleep(1);
	}
}

void	Game::_loadMap(const std::string &mapFile)
{
	if (this->_map)
		delete this->_map;
	this->_map = new Map(this->_device, this->_driver, this->_smgr, this->_camera, mapFile, this->_textureSize, this->_screenWidth, this->_screenHeight);
}
