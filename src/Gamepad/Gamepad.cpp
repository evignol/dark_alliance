#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <list>
#include "Gamepad.hpp"

Gamepad		*createController(irr::IrrlichtDevice *device, AController *controller)
{
	return new Gamepad(device, controller);
}

void		deleteController(Gamepad *controller)
{
	delete controller;
}

Gamepad::Gamepad(irr::IrrlichtDevice *device, AController *receiver) : _DEAD_ZONE(0.05f), _model_offset(0.0f), _joystickInfo(NULL), _device(device), _receiver(receiver), _player(NULL)
{
	for (int i = 0; i < 32; i++)
		this->_action_ptr[i] = NULL;
	device->activateJoysticks(this->_joystickInfo);
	this->loadConfig("gamepad.cfg");
}

Gamepad::~Gamepad(void)
{}

void	Gamepad::setPlayer(APlayer *player)
{
	this->_player = player;
}

void	Gamepad::melee_attack(irr::f32 val1, irr::f32 val2)
{
	if (this->_player)
		this->_player->melee_attack();
}

void	Gamepad::heavy_attack(irr::f32 val1, irr::f32 val2)
{
	if (this->_player)
		this->_player->heavy_attack();
}

void	Gamepad::magical_attack(irr::f32 val1, irr::f32 val2)
{
	if (this->_player)
		this->_player->magical_attack();
}

void	Gamepad::loadConfig(std::string const &cfgFile)
{
	int								val;
	std::ifstream					file(cfgFile.c_str(), std::ios::in);
	std::map<int, std::string>		list;
	std::string						tmp_str;

	if (!file)
		return;
	while (getline(file, tmp_str, '\n'))
	{
		std::string	key(tmp_str, 0, tmp_str.find('='));
		std::string	value(tmp_str, tmp_str.find('=') + 1, tmp_str.size());
		val = atoi(value.c_str());
		if (key == "light_attack")
			this->_action_ptr[val] = &Gamepad::melee_attack;
		if (key == "heavy_attack")
			this->_action_ptr[val] = &Gamepad::heavy_attack;
		if (key == "magical_attack")
			this->_action_ptr[val] = &Gamepad::magical_attack;
	}
	file.close();
}

void	Gamepad::refresh(void)
{
	irr::f32							horizontal = 0.f, vertical = 0.f, strafeHorizontal = 0.f, strafeVertical = 0.f;

	if (!this->_player || !this->_receiver)
		return;
	const irr::SEvent::SJoystickEvent	&joystickData = this->_receiver->GetJoystickState();
	horizontal = (irr::f32)joystickData.Axis[irr::SEvent::SJoystickEvent::AXIS_X] / 32767.f;
	strafeHorizontal = (irr::f32)joystickData.Axis[irr::SEvent::SJoystickEvent::AXIS_Z] / 32767.f;
	strafeVertical = (irr::f32)joystickData.Axis[irr::SEvent::SJoystickEvent::AXIS_R] / 32767.f;
	vertical = (irr::f32)joystickData.Axis[irr::SEvent::SJoystickEvent::AXIS_Y] / -32767.f;

	if (fabs(horizontal) < this->_DEAD_ZONE)
		horizontal = 0.f;
	if (fabs(vertical) < this->_DEAD_ZONE)
		vertical = 0.f;
	if (fabs(strafeVertical) < this->_DEAD_ZONE)
		strafeVertical = 0.f;
	if (fabs(strafeHorizontal) < this->_DEAD_ZONE)
		strafeHorizontal = 0.f;
	if (irr::core::equals(strafeHorizontal, -1.f) || irr::core::equals(strafeHorizontal, 1.f) || irr::core::equals(strafeVertical, -1.f) || irr::core::equals(strafeVertical, 1.f))
		this->_player->strafe(strafeHorizontal, strafeVertical);
	else
		this->_player->move(horizontal, vertical);
	for (int i = 0; i < 32; i++)
		if ((irr::u32)joystickData.IsButtonPressed(i) && this->_action_ptr[i])
			(this->*_action_ptr[i])(0.f, 0.f);
}
