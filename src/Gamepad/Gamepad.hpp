#ifndef GAMEPAD_HPP
# define GAMEPAD_HPP

# include <irrlicht.h>
# include <iostream>
# include <algorithm>
# include <map>
# include "../AController.hpp"
# include "../IController.hpp"
# include "../APlayer.hpp"

class	Gamepad : public IController
{
	public:
		Gamepad(irr::IrrlichtDevice *device, AController *receiver);
		~Gamepad(void);
		void									refresh(void);
		void									setPlayer(APlayer *player);
		void									loadConfig(std::string const &cfgFile);
		void									melee_attack(irr::f32 val1, irr::f32 val2);
		void									heavy_attack(irr::f32 val1, irr::f32 val2);
		void									magical_attack(irr::f32 val1, irr::f32 val2);

	private:
		void									(Gamepad::*_action_ptr[32])(irr::f32 val1, irr::f32 val2);
		const irr::f32							_DEAD_ZONE;
		irr::f32								_model_offset;
		irr::core::array<irr::SJoystickInfo>	_joystickInfo;
		irr::IrrlichtDevice						*_device;
		AController								*_receiver;
		APlayer									*_player;
};

extern "C"
{
	Gamepad										*createController(irr::IrrlichtDevice *device, AController *controller);
	void										deleteController(Gamepad *controller);
}

#endif