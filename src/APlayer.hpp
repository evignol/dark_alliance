#ifndef APLAYER_HPP
# define APLAYER_HPP

# include <irrlicht.h>

enum	State
{
	STATE_IDLE,
	STATE_WALK,
	STATE_RUN,
	STATE_ROTATE,
	STATE_STRAFE,
	STATE_LIGHT_ATTACK,
	STATE_HEAVY_ATTACK,
	STATE_MAGICAL_ATTACK
};

class	APlayer : public irr::scene::IAnimationEndCallBack
{
	public:
		APlayer(irr::IrrlichtDevice *device, irr::video::IVideoDriver *driver, irr::scene::ISceneManager *smgr, irr::scene::ICameraSceneNode *camera, const std::string &name, const irr::core::vector3df &pos, int textureSize);
		~APlayer(void);
		virtual void								refresh(void);
		virtual irr::f32							getOffset(void) const;
		virtual irr::core::vector3df				getCollOffset(void) const;
		virtual irr::core::vector3df				getCollRadius(void) const;
		virtual void								move(irr::f32 horizontal, irr::f32 vertical);
		virtual void								strafe(irr::f32 horizontal, irr::f32 vertical);
		virtual void								melee_attack(void);
		virtual void								heavy_attack(void);
		virtual void								magical_attack(void);
		virtual void								OnAnimationEnd(irr::scene::IAnimatedMeshSceneNode* node);
		virtual irr::scene::IAnimatedMeshSceneNode	*getNode(void) const;
		virtual void								setPos(const irr::core::vector3df &pos);
		virtual irr::core::vector3df				getPos(void) const;

	private:
		int											_FPS;
		irr::f32									_MOVEMENT_SPEED;
		irr::f32									_offset;
		irr::core::vector2di						_animIdle, _animWalk, _animRun, _animStrafe, _animMeleeAttack, _animHeavyAttack, _animMagicalAttack;
		irr::core::vector3df						_collOffset, _collRadius;
		irr::IrrlichtDevice							*_device;
		irr::video::IVideoDriver					*_driver;
		irr::scene::ISceneManager					*_smgr;
		irr::scene::ICameraSceneNode				*_camera;
		irr::scene::IAnimatedMesh					*_mesh;
		irr::scene::IAnimatedMeshSceneNode			*_node;
		irr::video::ITexture						*_diffuse, *_normal;
		irr::u32									_then;
		irr::f32									_oldHoryzontal, _oldVertical;
		State										_state;
};

#endif
