#ifndef CONTROLLER_HPP
# define CONTROLLER_HPP

# include <irrlicht.h>

class	AController : public irr::IEventReceiver
{
	public:
		// We'll create a struct to record info on the mouse state
		struct SMouseState
		{
			irr::core::position2di Position;
			bool LeftButtonDown;
			SMouseState() : LeftButtonDown(false) { }
		} MouseState;

		// This is the one method that we have to implement
		virtual bool OnEvent(const irr::SEvent& event)
		{
			if (event.EventType == irr::EET_KEY_INPUT_EVENT)
			{
				KeyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
			}

			// Remember the mouse state
			if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
			{
				switch(event.MouseInput.Event)
				{
				case irr::EMIE_LMOUSE_PRESSED_DOWN:
					MouseState.LeftButtonDown = true;
					break;

				case irr::EMIE_LMOUSE_LEFT_UP:
					MouseState.LeftButtonDown = false;
					break;

				case irr::EMIE_MOUSE_MOVED:
					MouseState.Position.X = event.MouseInput.X;
					MouseState.Position.Y = event.MouseInput.Y;
					break;

				default:
					// We won't use the wheel
					break;
				}
			}

			// The state of each connected joystick is sent to us
			// once every run() of the Irrlicht device.  Store the
			// state of the first joystick, ignoring other joysticks.
			// This is currently only supported on Windows and Linux.
			if (event.EventType == irr::EET_JOYSTICK_INPUT_EVENT
				&& event.JoystickEvent.Joystick == 0)
			{
				JoystickState = event.JoystickEvent;
			}
			return false;
		}

		const irr::SEvent::SJoystickEvent & GetJoystickState(void) const
		{
			return JoystickState;
		}

		const SMouseState & GetMouseState(void) const
		{
			return MouseState;
		}

		virtual bool IsKeyDown(irr::EKEY_CODE keyCode) const
		{
			return KeyIsDown[keyCode];
		}

		AController()
		{
			for (irr::u32 i = 0; i < irr::KEY_KEY_CODES_COUNT; ++i)
				KeyIsDown[i] = false;
		}

		bool							KeyIsDown[irr::KEY_KEY_CODES_COUNT];

	private:
		irr::SEvent::SJoystickEvent		JoystickState;
};

#endif