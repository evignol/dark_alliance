Target =			dark_alliance
PS3GamepadTarget =	PS3Gamepad.so
Sources =			main.cpp			\
					APlayer.cpp			\
					AObject.cpp			\
					cfgParser.cpp		\
					Map.cpp				\
					textureLoader.cpp	\
					Game.cpp
PS3Sources =		Gamepad.cpp

OBJ = $(Sources:.cpp=.o)
SRCDIR = $(addprefix src/,$(OBJ))
PS3OBJ = $(PS3Sources:.cpp=.o)
PS3GAMEPADSRCDIR = $(addprefix src/Gamepad/,$(PS3OBJ))
CPPFLAGS = -std=c++11 -I../irrlicht/include -I/usr/X11R6/include
CXXFLAGS = -g -ffast-math -w
LDFLAGS = -L/usr/X11R6/lib$(LIBSELECT) -L../irrlicht/lib/Linux -lIrrlicht -lGL -lXxf86vm -lXext -lX11 -lXcursor -ldl

all: $(PS3GamepadTarget) $(Target)
$(Target): $(SRCDIR)
	@g++ $^ -o $@ $(LDFLAGS)
	@echo "Dark Alliance ready"

$(PS3GamepadTarget): $(PS3GAMEPADSRCDIR)
	@g++ $^ -o $@ -shared -fPIC $(LDFLAGS)
	@echo "Gamepad ready"

%.o: %.cpp
	@echo "Dark Alliance: compiling " $<
	@g++ $(CPPFLAGS) $(CXXFLAGS) $< -o $@ -c -fPIC

clean:
	@$(RM) -f $(SRCDIR)
	@$(RM) -f $(PS3GAMEPADSRCDIR)

fclean: clean
	@$(RM) $(Target)
	@$(RM) $(PS3GamepadTarget)

re: fclean all

.PHONY: all clean fclean
